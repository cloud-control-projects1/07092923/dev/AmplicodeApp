package com.company.amplicode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class AmplicodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmplicodeApplication.class, args);
    }
}
