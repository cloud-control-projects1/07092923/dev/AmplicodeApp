variable "name" {
  type        = string
  default     = "amplicode"
  description = "Application name"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "use_private_subnets" {
  type    = bool
  default = false
}

variable "azs_count" {
  type        = number
default     = 2
    description = "Number of Availability Zones to be used"
}

variable "instance_type" {
  type    = string
  default = "t2.medium"
}

variable "region" {
  type    = string
  default = "us-east-2"
}

variable "image" {
  type    = string
}

variable "ports" {
  type    = list(number)
  default = [ 8080 ]
}

locals {
  env = [
          {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "AMPLICODE_DB_NAME",
        value = module.amplicode_db.name
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "AMPLICODE_DB_HOST",
        value = module.amplicode_db.host
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "AMPLICODE_DB_PORT",
        value = module.amplicode_db.port
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "AMPLICODE_DB_USER",
        value = module.amplicode_db.username
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "AMPLICODE_DB_PASSWORD",
        value = module.amplicode_db.password
      },
              {
        namespace = "aws:elbv2:listener:443",
        name = "ListenerEnabled",
        value = "true"
      },
      {
        namespace = "aws:elbv2:listener:443",
        name = "Protocol",
        value = "HTTPS"
      },
      {
        namespace = "aws:elbv2:listener:443",
        name = "SSLCertificateArns",
        value = module.ssl.certificate_arn
      },
              {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "StreamLogs"
        value = var.enable_logging
      },
      {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "DeleteOnTerminate"
        value = var.delete_logs_on_terminate
      },
      {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "RetentionInDays"
        value = var.logs_retention
      },
              {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "SPRING_PROFILES_ACTIVE",
        value = "aws"
      }
      ]
}

variable "enable_logging" {
  type    = bool
  default = true
}

variable "delete_logs_on_terminate" {
  type    = bool
  default = true
}

variable "logs_retention" {
  type    = number
  default = 30
}

variable "amplicode_db_name" {
  type    = string
  default = "amplicode"
}

variable "amplicode_db_engine" {
  type    = string
  default = "postgres"
}

variable "amplicode_db_engine_version" {
  type    = string
  default = "15.3"
}

variable "amplicode_db_instance_class" {
  type    = string
  default = "db.t3.small"
}

variable "amplicode_db_storage" {
  type    = number
  default = 10
}

variable "amplicode_db_user" {
  type    = string
  default = "root"
}

variable "amplicode_db_password" {
  type  = string
default = null
  }
      
variable "amplicode_db_random_password" {
  type    = bool
  default = true
}

variable "amplicode_db_multi_az" {
  type    = bool
  default = false
}

    