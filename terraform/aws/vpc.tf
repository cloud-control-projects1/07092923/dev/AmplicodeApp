module "vpc" {
  source = "./vpc"

  azs_count           = var.azs_count
  name                = var.name
  use_private_subnets = var.use_private_subnets
  vpc_cidr_block      = var.vpc_cidr_block
  use_db              = true
}